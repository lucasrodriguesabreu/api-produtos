package br.com.lucas.apiprodutos.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor

public class ResourceNotFoundDetails {

    private String titulo;
    private int status;
    private String detalhe;
    private long timestamp;
    private String mensagemDesenvolvedor;

    public static final class Builder {
        private String titulo;
        private int status;
        private String detalhe;
        private long timestamp;
        private String mensagemDesenvolvedor;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder titulo(String titulo) {
            this.titulo = titulo;
            return this;
        }

        public Builder status(int status) {
            this.status = status;
            return this;
        }

        public Builder detalhe(String detalhe) {
            this.detalhe = detalhe;
            return this;
        }

        public Builder timestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder mensagemDesenvolvedor(String mensagemDesenvolvedor) {
            this.mensagemDesenvolvedor = mensagemDesenvolvedor;
            return this;
        }

        public ResourceNotFoundDetails build() {
            ResourceNotFoundDetails resourceNotFoundDetails = new ResourceNotFoundDetails();
            resourceNotFoundDetails.setTitulo(titulo);
            resourceNotFoundDetails.setStatus(status);
            resourceNotFoundDetails.setDetalhe(detalhe);
            resourceNotFoundDetails.setTimestamp(timestamp);
            resourceNotFoundDetails.setMensagemDesenvolvedor(mensagemDesenvolvedor);
            return resourceNotFoundDetails;
        }
    }
}
