package br.com.lucas.apiprodutos.service;

import br.com.lucas.apiprodutos.error.ResourceNotFoundException;
import br.com.lucas.apiprodutos.models.Produto;
import br.com.lucas.apiprodutos.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto atualizaProduto(long id, Produto produto) {
        Produto produto1 = produtoRepository.findById(id);
        produto1.setNome(produto.getNome());
        produto1.setQuantidade(produto.getQuantidade());
        produto1.setValor(produto.getValor());

        Produto produtoSalvo = produtoRepository.save(produto1);
        return produtoSalvo;
    }

    public void deletaProdutoById(@PathVariable(value = "id") long id){
        Produto produto = new Produto();
        produto.setId(id);
        produtoRepository.delete(produto);
    }

    public List<Produto> listaProdutos(){
        return this.produtoRepository.findAll();
    }

    public Produto salvaProduto(@PathVariable Produto produto){
        return produtoRepository.save(produto);
    }

    public Produto listaProdutoUnico(@PathVariable(value="id") long id) {
        return produtoRepository.findById(id);
    }
}