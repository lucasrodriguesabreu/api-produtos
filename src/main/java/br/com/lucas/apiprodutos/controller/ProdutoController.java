package br.com.lucas.apiprodutos.controller;

import br.com.lucas.apiprodutos.error.ResourceNotFoundException;
import br.com.lucas.apiprodutos.models.Produto;
import br.com.lucas.apiprodutos.repository.ProdutoRepository;
import br.com.lucas.apiprodutos.service.ProdutoService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional.*;
//classe que recebe as requisições http

@RestController
@RequestMapping(value="/api")
public class ProdutoController {

    //ponto de injeção
    @Autowired
    ProdutoService produtoService;

    //método get para listar os produtos salvos no banco de dados
    @GetMapping("/produtos")
    public List<Produto> listaProdutos(){
        return produtoService.listaProdutos();
    }

    //método get para listar os produtos salvos no banco de dados
    @GetMapping("/produto/{id}")
    public Produto listaProdutoUnico(@PathVariable(value="id") long id){
        verificaSeProdutoExiste(id);
        return produtoService.listaProdutoUnico(id);
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/produto")
    //@RequestBody, pois vem no corpo da requisição
    public Produto salvaProduto(@RequestBody Produto produto){
        return produtoService.salvaProduto(produto);
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping("/produto/{id}")
    public void deletaProdutoById(@PathVariable(value = "id") long id){
        verificaSeProdutoExiste(id);
        produtoService.deletaProdutoById(id);
    }

    @PutMapping("/produto/{id}")
    public Produto atualizaProduto(@PathVariable (value="id") long id, @RequestBody Produto produto){
        verificaSeProdutoExiste(produto.getId());
        return produtoService.atualizaProduto(id, produto);
    }


    private void verificaSeProdutoExiste(long id){
        if(listaProdutoUnico(id) == null)
            throw new ResourceNotFoundException("Produto não encontrado, o ID digitado foi " + id);
    }

}