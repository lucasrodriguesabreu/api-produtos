package br.com.lucas.apiprodutos.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

//entity pois é uma entidade no banco de dados
@Entity
//@table = personalizei o nome da tabela, caso não faça isso é salvo no banco de dados com o nome da tabela
@Table(name="tb_produto")
//usado para o lombok
@Data
@Getter @Setter @AllArgsConstructor @NoArgsConstructor @Builder

public class Produto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) //indicando ao banco que os id's serão criados automaticamente
    private long id;

    private String nome;
    private int quantidade;
    private double valor;

}
