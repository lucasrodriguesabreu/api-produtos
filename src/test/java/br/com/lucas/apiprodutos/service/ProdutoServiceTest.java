package br.com.lucas.apiprodutos.service;

import br.com.lucas.apiprodutos.controller.ProdutoController;
import br.com.lucas.apiprodutos.models.Produto;
import br.com.lucas.apiprodutos.repository.ProdutoRepository;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)

class ProdutoServiceTest {

    @Mock
    ProdutoRepository produtoRepository;

//    @InjectMocks
//    ProdutoService produtoService;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    ProdutoController produtoController;


    Produto produto = Produto.builder()
            .nome("Samsung s9")
            .quantidade(1)
            .valor(2000.00)
            .build();

    Produto produto2 = Produto.builder()
            .nome("Samsung s10")
            .quantidade(1)
            .valor(3000.00)
            .build();

    List<Produto> listaProduto = new ArrayList<>();
    @Test
    void atualizaProduto() {
    }

    @Test
    void deletaProdutoById() {
    }

    @Test
    void listaProdutos() {
        listaProduto.add(produto);
        when(produtoRepository.findAll()).thenReturn(listaProduto);
        var expected = produtoService.listaProdutos();
        var atual = listaProduto;
        assertEquals(expected, atual);
    }

    @Test
    void salvaProduto() {
    }
/*
    @Test
    void listaProdutoUnico() {
        listaProduto.add(produto);
        when(this.produtoRepository.findById(1L)).thenReturn(produto);
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/produto/{id}", produto.getId())
                .then()
                .statusCode(HttpStatus.OK.value());

    }*/

    @Test
    void listaProdutoUnido(){
        listaProduto.add(produto);
        when(produtoRepository.findById(1L)).thenReturn(listaProduto);
    }
}